require_relative './node'

class RangeList

  def initialize
    @root = nil
  end

  def add(range)
    if range.is_a?(Array) && range.first <= range.last
      (range.first...range.last).each do |item|
        @root = insert(@root, item)
      end
    end
  end

  def remove(range)
    if range.is_a?(Array) && range.first <= range.last
      (range.first...range.last).each do |item|
        @root = remove_item(@root, item)
      end
    end
  end

  def print
    return "[]" if @root == nil
    str_array = []
    prev_item = nil
    callback = lambda do |node|
      return if node == nil
      value = node.value
      if prev_item == nil
        prev_item = value
        str_array.push("[#{value}")
      elsif value - prev_item == 1
        prev_item = value
      else
        str_array[str_array.length - 1] +=  ", #{prev_item + 1 })"
        str_array.push("[#{value}")
        prev_item = value
      end
    end
    inorder_tree_walk(@root, callback)
    str_array[str_array.length - 1] += ", #{prev_item + 1})"
    return str_array.join(" ")
  end

  def inorder_tree_walk(node, callback)
    return if node == nil
    callback.call(inorder_tree_walk(node.left, callback)) if node.left != nil
    callback.call(node)
    callback.call(inorder_tree_walk(node.right, callback)) if node.right != nil
  end

  private
    def height(node)
      node == nil ? -1 : node.height
    end

    def update_height(node)
      return [height(node.left), height(node.right)].max + 1
    end

    def rotate_right(node)
      left_node = node.left
      node.left = left_node.right
      left_node.right = node
      node.height = update_height(node)
      left_node.height = update_height(left_node)
      return left_node
    end

    def rotate_left(node)
      right_node = node.right
      node.right = right_node.left
      right_node.left = node
      node.height = update_height(node)
      right_node.height = update_height(right_node)
      return right_node
    end

    def rotate_left_right(node)
      node.left = rotate_left(node.left)
      return rotate_right(node)
    end

    def rotate_right_left(node)
      node.right = rotate_right(node.right)
      return rotate_left(node)
    end

    def insert(node, item)
      return Node.new(item) if node == nil
      if item < node.value
        node.left = insert(node.left, item)
        if height(node.left) - height(node.right) == 2
          if item < node.left.value
            node = rotate_right(node)
          else
            node = rotate_left_right(node)
          end
        end
      elsif item > node.value
        node.right = insert(node.right, item)
        if height(node.right) - height(node.left) == 2
          if item > node.right.value
            node = rotate_left(node)
          else
            node = rotate_right_left(node)
          end
        end
      end
      node.height = update_height(node)
      return node
    end

    def remove_item(node, item)
      return nil if node == nil
      if item < node.value
        node.left = remove_item(node.left, item)
        if height(node.right) - height(node.left) == 2
          node = rotate_left(node)
        end
      elsif item > node.value
        node.right = remove_item(node.right, item)
        if height(node.left) - height(node.right) == 2
          node = rotate_right(node)
        end
      else
        return node.right if node.left == nil
        return node.left if node.right == nil
        min_node = get_min_node(node.right)
        min_node.right = remove_min_node(node.right)
        min_node.left = node.left
        node = min_node
        if height(node.left) - height(node.right) == 2
          node = rotate_right(node)
        end
      end
      node.height = update_height(node)
      return node
    end

    def get_min_node(node)
      return node if node.left == nil
      return get_min_node(node.left)
    end

    def remove_min_node(node)
      return nil if node == nil
      return node.right if node.left == nil
      node.left = remove_min_node(node.left)
      if height(node.right) - height(node.left) == 2
        node = rotate_left(node)
      end
      return node
    end

end