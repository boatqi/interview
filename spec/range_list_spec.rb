require_relative '../range_list.rb'

RSpec.describe RangeList do
  before do
    @range_list = RangeList.new
  end

  describe "#add range" do
    it "Add Frist range" do
      @range_list.add([1, 5])
      expect(@range_list.print).to eq("[1, 5)")
    end

    it "Add multiple range" do
      @range_list.add([1, 5])
      expect(@range_list.print).to eq("[1, 5)")
      @range_list.add([10, 20])
      expect(@range_list.print).to eq("[1, 5) [10, 20)")
      @range_list.add([20, 20])
      expect(@range_list.print).to eq("[1, 5) [10, 20)")
    end
  end

  describe "#remove range" do
    it "remove one node" do
      @range_list.add([1, 5])
      expect(@range_list.print).to eq("[1, 5)")
      @range_list.remove([3, 4])
      expect(@range_list.print).to eq("[1, 3) [4, 5)")
    end

    it "remove multiple node" do
      @range_list.add([1, 10])
      expect(@range_list.print).to eq("[1, 10)")
      @range_list.remove([5, 7])
      expect(@range_list.print).to eq("[1, 5) [7, 10)")
    end
  end

  describe "#Full test " do

    it "#Questions test" do
      @range_list.add([1, 5])
      expect(@range_list.print).to eq("[1, 5)")

      @range_list.add([10, 20])
      expect(@range_list.print).to eq("[1, 5) [10, 20)")

      @range_list.add([20, 20])
      expect(@range_list.print).to eq("[1, 5) [10, 20)")

      @range_list.add([20, 21])
      expect(@range_list.print).to eq("[1, 5) [10, 21)")

      @range_list.add([2, 4])
      expect(@range_list.print).to eq("[1, 5) [10, 21)")

      @range_list.add([3, 8])
      expect(@range_list.print).to eq("[1, 8) [10, 21)")

      @range_list.remove([10, 10])
      expect(@range_list.print).to eq("[1, 8) [10, 21)")

      @range_list.remove([10, 11])
      expect(@range_list.print).to eq("[1, 8) [11, 21)")

      @range_list.remove([15, 17])
      expect(@range_list.print).to eq("[1, 8) [11, 15) [17, 21)")
      @range_list.remove([3, 19])
      expect(@range_list.print).to eq("[1, 3) [19, 21)")
    end

  end
end