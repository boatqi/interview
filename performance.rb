require 'benchmark'
require_relative './range_list.rb'

puts "Add performance"
range_list = RangeList.new
Benchmark.bm do |x|
  x.report { range_list.add([1, 10000]) }
  x.report { range_list.add([1, 100000]) }
  x.report { range_list.add([1, 1000000]) }
end

puts "Remove performance"
range_list = RangeList.new
range_list.add([1, 2000000])
Benchmark.bm do |x|
  x.report { range_list.remove([1, 10000]) }
  x.report { range_list.remove([1, 100000]) }
  x.report { range_list.remove([1, 1000000]) }
end

puts "Print performance"
rl1 = RangeList.new
rl2 = RangeList.new
rl3 = RangeList.new

rl1.add([1, 10000])
rl2.add([1, 100000])
rl3.add([1, 1000000])
Benchmark.bm do |x|
  x.report { rl1.print }
  x.report { rl2.print }
  x.report { rl3.print }
end

puts "Random number add performance"
rl = RangeList.new
Benchmark.bm(7) do |x|
  x.report do
    100000.times do
      num = rand(1..1000000)
      rl.add([num, num + 1])
    end
    100000.times do
      num = rand(1..1000000)
      rl.remove([num, num + 1])
    end
  end
end

puts "Random number remove performance"
rl = RangeList.new
rl.add([1, 100000])
Benchmark.bm(7) do |x|
  x.report do
    100000.times do
      num = rand(1..1000000)
      rl.remove([num, num + 1])
    end
  end
end