class Node
  attr_accessor :left, :right, :value, :height
  def initialize(item)
    @left = nil
    @right = nil
    @value = item
    @height = 0
  end

  def to_s
    "value: #{@value}, left: #{left&.value}, right: #{right&.value}, height: #{@height}"
  end

end